FROM registry.gitlab.com/attakei.net/slides-workspace as build

RUN mkdir -p /workspace/{src,dist}
COPY ./ /workspace/src
RUN pip install -r /workspace/src/requirements.txt \
    && pip install sphinx-revealjs==0.12.0 \
    && sphinx-build -b revealjs /workspace/src /workspace/dist

FROM nginx:stable-alpine
ARG SLIDE_NAME
COPY --from=build /workspace/dist/ /usr/share/nginx/html
RUN mkdir -p /var/www/html && { \
    echo "User-agent: *"; \
    echo "Disallow: /"; \
} > /var/www/html/robots.txt;
RUN { \
    echo "server {"; \
    echo "  listen      8080;"; \
    echo "  server_name slide-server;"; \
    echo "  root /var/www/html;"; \
    echo "  location /slides/$SLIDE_NAME {"; \
    echo "    alias /usr/share/nginx/html;"; \
    echo "    index index.html;"; \
    echo "  }"; \
    echo "  error_page 500 502 503 504 /50x.html;"; \
    echo "  location = /50x.html {"; \
    echo "    root /usr/share/nginx/html;"; \
    echo "  }"; \
    echo "}"; \
} > /etc/nginx/conf.d/default.conf
