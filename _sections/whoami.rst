自己紹介
--------

.. container:: flex

    .. container:: two-of-third

        Kazuya Takei

        * NIJIBOX Co., Ltd
        * Server-side engineer

        @attakei

        * Pythonista
        * sphinx-revealjs and more

    .. container:: one-of-third

        .. figure:: https://attakei.net/_static/images/icon-attakei@2x.jpg